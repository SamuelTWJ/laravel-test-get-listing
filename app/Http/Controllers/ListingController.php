<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Listing;
use App\User;
use App\Http\Requests\ListingFormValidation;

class ListingController extends Controller
{
    public function __construct()
    {
        
        $this->listing = new Listing;
        $this->user = new User;
    }

    public function index(Request $request)
    {
        if ($request->ajax()) 
        {
            $data = $this->listing->getListings();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                    
                        $action = '
                        <a class="btn btn-success edit-listing" id="edit-listing" data-toggle="tooltip" title="Edit Listing" data-id='.$row->id.'>Edit</a>

                        <a id="delete-listing" data-id='.$row->id.' class="btn btn-danger delete-listing" data-toggle="tooltip" title="Delete User">Delete</a>';
                        
                        return $action;
                    
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }

        $users = $this->user->getUsers();

        return view('listing', compact('users'));
    }

    public function edit($id)
    {
        $listing = $this->listing->getListings($id);
        return response()->json($listing);
    }

    public function destroy($id)
    {
        $this->listing->deleteListing($id);
        return redirect()->route('listing.index');
    }

    public function show(Request $request)
    {
        if($request->id != auth()->user()->id)
            return response()->json([
                'status' => 403,
                'message' => 'Unauthorized Access.'
            ]);
        else
        {
            if(($request->latitude<-90 || $request->latitude>90) || (($request->longitude<-180 || $request->longitude > 180)))
            {
                return response()->json([
                    'status' => 400,
                    'message' => 'Invalid latitude/longitude value'
                ]);
            }
            else
            {
                $listings = $this->listing->getListings($request->id);
                $data = [];
                foreach($listings as $key=>$listing)
                {
                    $distance = $this->getDistance($request->latitude, $request->longitude, $listing->latitude, $listing->longitude);
                    $data[$key] = ['id' => $listing->id,
                    'name' => $listing->name,
                    'distance' => $distance,
                    'created_at' => $listing->created_at,
                    'updated_at' => $listing->updated_at];
                }
                return response()->json([
                    'status' => 200,
                    'message' => 'Success',
                    'result' => [
                        'data' => $data
                    ]
                ]);
            }
        }
            
    }

    public function getDistance($user_latitude, $user_longitude, $listing_latitude, $listing_longitude)
    {
        $earthRadius = 6371000;
        $usrLat = deg2rad($user_latitude);
        $usrLong = deg2rad($user_longitude);
        $listLat = deg2rad($listing_latitude);
        $listLong = deg2rad($listing_longitude);

        $lonDelta = $listLong - $usrLong;
        $a = pow(cos($listLat) * sin($lonDelta), 2) +
            pow(cos($usrLat) * sin($listLat) - sin($usrLat) * cos($listLat) * cos($lonDelta), 2);
        $b = sin($usrLat) * sin($listLat) + cos($usrLat) * cos($listLat) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);
        $distance = number_format(($angle * $earthRadius)/1000,2);
        return $distance;
    }

    public function store(ListingFormValidation $request)
    {      
        $dataToUpdate = 
        [
            'name' => $request->name,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'user_id' => $request->user,
        ];

        $this->listing->insertNewListing($request->listing_id, $dataToUpdate);
        
        if($request->listing_id)
            $msg = 'User updated successfully.';
        else
            $msg = 'User created successfully.';

        return redirect()->route('listing.index');
    }
}
