<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $this->user = new User;
        try 
        {
            $request->validate([
            'email' => 'email|required',
            'password' => 'required'
            ]);

            $credentials = request(['email', 'password']);

            if (!Auth::attempt($credentials)) 
            {
                return response()->json([
                    'status' => 500,
                    'message' => 'Unauthorized Access'
                ]);
            }

            $user = $this->user->findEmail($request->email);

            if ( ! Hash::check($request->password, $user->password, [])) 
            {
                throw new \Exception('Error in Login');
            }

            $tokenResult = $user->createToken('authToken');

            return response()->json([
            'status' => 200,
            'message' => 'Logged in',
            'result' => [
                'user_id' => $user->id,
                'access_token' => $tokenResult->plainTextToken,
                'token_type' => 'Bearer',
                'role type' => $user->role_type,
            ]
            
            ]);
        } 
        catch (Exception $error) 
        {
            return response()->json([
            'status' => 500,
            'message' => 'Error in Login',
            'error' => $error,
            ]);
        }
    }
}
