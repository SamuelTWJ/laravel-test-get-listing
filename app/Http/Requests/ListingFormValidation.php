<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ListingFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request('listing_id');

        $dataToValidate = [
            'name' => 'required|string|max:255',
            'latitude' => 'required|numeric|between:-90,90',
            'longitude' => 'required|numeric|between:-180,180',
            'user' => 'required',
        ];

        return $dataToValidate;
    }

    public function messages()
    {
        return [
            'name.required'=>'Please enter a name for the listing.',
            'latitude.numeric'=>'The latitude must be numerical.',
            'longitude.numeric'=>'The longitude must be numerical.',
            'longitude.between'=>'The longitude must be between -180 and 180.',
            'latitude.between'=>'The latitude must be between -90 and 90.',
            'user.required'=>'Please select a user.',
        ];
    }
}
