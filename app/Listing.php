<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{
    protected $guarded = [];

    public function getListings($user_id = null)
    {
        if($user_id == null)
            return $this->join('users', 'users.id', '=', 'listings.user_id')->select('users.name as username', 'listings.*')->get();
        else
            return $this->where('user_id', $user_id)->get();
    }

    public function insertNewListing($listing_id, $data)
    {
        if($listing_id)
        {
            $this->find($listing_id)->update($data);
        }
        else
        {
            $this->create($data);
        }
    }

    public function deleteListing($id)
    {
        return $this->find($id)->delete();
    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
