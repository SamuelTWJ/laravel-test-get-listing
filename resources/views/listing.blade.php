@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="col-md-12">
                        <h4 class="card-title">Listings
                          <a class="btn btn-success ml-5" href="javascript:void(0)" id="newlisting" title="Add Listing">Add Listing</a>
                        </h4>
                    </div>
                 </div>
                <div class="card-body">
                    <table class="table listing-table">
                         <thead>
                             <tr>
                                 <th width="5%">No</th>
                                 <th>Name</th>
                                 <th>Latitude</th>
                                 <th>Longitude</th>
                                 <th>User</th>
                                 <th>Created At</th>
                                 <th width="13%">Action</th>
                             </tr>
                         </thead>
                         <tbody>
                         </tbody>
                     </table>
                 </div>

                <div class="modal fade" id="ajaxModel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="modelHeading"></h4>
                            </div>
                            <div class="modal-body">
                                <form id="ListingForm" name="ListingForm" class="form-horizontal">
                                    <input type="hidden" name="listing_id" id="listing_id">
                                    <div class="form-group">
                                        <label for="name" class="col-sm-2 control-label">Name</label>
                                        <div class="col-sm-12">
                                            <input type="text" id="name" name="name" placeholder="Enter Name" value="" maxlength="50" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Latitude</label>
                                        <div class="col-sm-12">
                                            <input type="text" id="latitude" name="latitude" placeholder="Enter Latitude" value="" maxlength="50" required>
                                        </div>
                                    </div>
        
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label">Longtitude</label>
                                        <div class="col-sm-12">

                                            <input type="text" id="longitude" name="longitude" placeholder="Enter Longtitude" value="" maxlength="50" required>
                                        </div>
                                    </div>
        
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label">User</label>
                                        <div class="col-sm-12">
                                            <select id="user" name="user">
                                                <option value="" selected>Select User</option>
                                                @foreach ($users as $user)
                                                    <option value={{$user->id}}>{{$user->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
        
                                    <div class="col-sm-offset-2 col-sm-10">
                                     <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes
                                     </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
   
        $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
        });
   
        //Initialize and populate datatable with data from login table
        var table = $('.listing-table').DataTable({
            processing: false,
            serverSide: true,
            ajax: "{{ route('listing.index') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex',searchable: false},
                {data: 'name', name: 'name'},
                {data: 'latitude', name: 'latitude'},
                {data: 'longitude', name: 'longitude'},
                {data: 'username', name: 'user'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            columnDefs:[
                {targets:5, render:function(data){
                    return moment(data).format('DD MMM YYYY HH:mm')}}
            ]
        });
   
        $('#newlisting').click(function () {
            $('.text-danger').remove();
            $('#saveBtn').val("register-admin");
            $('#name').val('');
            $('#latitude').val('');
            $('#longitude').val('');
            $('#user').val('');
            $('#ListingForm').trigger("reset");
            $('#modelHeading').html("Add New Listing");
            $('#ajaxModel').modal('show');
        });
   
        $('body').on('click', '.edit-listing', function () {
            $('.text-danger').remove();
            var id = $(this).data('id');
            $.get("{{ route('listing.index') }}" +'/' + id +'/edit', function (data) {
                $('#modelHeading').html("Edit Listing");
                $('#saveBtn').val("edit-listing");
                $('#ajaxModel').modal('show');
   
                $('#listing_id').val(data.id);
                $('#name').val(data.name);
                $('#latitude').val(data.latitude);
                $('#longitude').val(data.longitude);
                $('#user').val(data.user_id);
          })
       });
   
        $('#saveBtn').click(function (e) {
            e.preventDefault();
            $('.text-danger').remove();
            $(this).html('Sending..');
   
            $.ajax({
              data: $('#ListingForm').serialize(),
              url: "{{ route('listing.store') }}",
              type: "POST",
              dataType: 'json',
              success: function (data) {
                  $('#ListingForm').trigger("reset");
                  $('#ajaxModel').modal('hide');
                  $('#saveBtn').html('Save Changes');
                  location.reload();
   
              },
              error: function (data) {
                  //Get error in the form of JSON
                  const errors = data.responseJSON.errors;
                  //Seperate name of error and description of error
                  const itemName = Object.keys(errors)[0];
                  const itemError = errors[itemName][0];
                  const itemDOM = document.getElementById(itemName);
                  //show error message
                  itemDOM.insertAdjacentHTML('afterend', '<div class = "text-danger">'+ itemError+'</div>');
                  $('#saveBtn').html('Save Changes');
              }
          });
        });
   
        $('body').on('click', '.delete-listing', function () {
   
         var listing_id = $(this).data("id");
   
         Swal.fire
         ({
            title: 'Do you want to delete the listing?',
            showDenyButton: true,
            showCancelButton: false,
            confirmButtonText: `Delete`,
            denyButtonText: `Don't Delete`,
        }).then((willDelete) => {
            if (willDelete.isConfirmed) {
                $.ajax({
                    type: "POST",
                    data: {_method:"DELETE"} ,
                    url: "{{ route('listing.store') }}"+'/'+listing_id,
                    success: function (data) {
                       location.reload();
                    }
                });
            }
            });
   
     });
   
      });
   </script>
@endsection
